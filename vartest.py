
import numpy
import pandas
from statsmodels.tsa.api import VAR
import statsmodels.api as sm
from statsmodels.tsa.base.datetools import dates_from_str

def  __make_bitvector(df, key, start, end):
    ret = pandas.DataFrame()
    
    for i in range(start, end + 1):
        dummy = (df[key] == i) + 0
        dummy.name = key + '_' + str(i)
        ret = pandas.concat([ret, dummy], axis=1)
        continue
    return ret

#df = pandas.read_csv('input.csv')
df=sm.datasets.macrodata.load_pandas().data

dates = df[['year', 'quarter']].astype(int).astype(str)
quarterly = dates["year"] + "Q" + dates["quarter"]
quarterly = dates_from_str(quarterly)
df.index = pandas.DatetimeIndex(quarterly)

work = __make_bitvector(df, 'quarter', 1, 4)
#data = pandas.concat([df[['realgdp']], work], axis=1)
data = pandas.concat([df[['realgdp']], work[['quarter_1', 'quarter_2']] ], axis=1)
print(data.head())

model=VAR(data)
#hint = model.select_order()
#print(hint)
result = model.fit(4)
#print(result.summary())
#result.test_causality('realgdp', ['quarter_1', 'quarter_2', 'quarter_3', 'quarter_4'])
result.test_causality('realgdp', ['quarter_1'])


 